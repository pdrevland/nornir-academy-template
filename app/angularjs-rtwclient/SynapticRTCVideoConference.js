/**
 * Created by pdrevland on 21.07.16.
 */

function SynapticRTCVideoConference(object) {
    var synapticNDS = object.snds;
    var channel = object.channel;
    var senderChannel = object.senderChannel;
    var sender = object.sender;
    var localVideo = document.getElementById(object.localStream) || null;
    var remoteStream = document.getElementById(object.remoteStream) || null;
    var localStream;
    var to;
    var divider = "¤£¤";
    var broadcastStream;
    var broadcastReceivers = [];

    var signal = new SynapticFileTransferSender(synapticNDS, channel, sender, function(){});
    var receiver = new SynapticFileTransferReceiver(synapticNDS, channel, sender, function(message) {
        var event = JSON.parse(message);
        console.log(event);
    });

    var send;


    var mediaConstraints = {
        audio: true,            // We want an audio track
        video: true             // ...and we want a video track
    };
    var pc;
    var hasAddTrack = false;
    var offer = true;
    var nic = false;
    var nicArr = [];
    var videoOfferReceived = [];
    var offerCounter = 0;
    var answerCounter = 0;
    var videoAnswerReceived = [];


    var signaler = {
        send: function(ev) {
            if (ev.type === "nic") {
                ev.from = sender;
                synapticNDS.send(channel,JSON.stringify(ev),to.toLowerCase());
                send.send(JSON.stringify(ev));
            }
            else {
                var length;
                var indexes;
                var remainder;
                var pos = 0;
                var end = 0;
                var size = 500;
                var type;
                if (ev.type === "video-offer") {
                    type = "o";
                    if (!offer) {
                        return;
                    }
                }
                else {
                    type = "a"
                }
                var str = JSON.stringify(ev);
                length = str.length;
                indexes = Math.floor(length / size);
                remainder = indexes % length;
                if (remainder) {
                    indexes++
                }
                for (var i = 0; i < indexes; i++) {
                    pos = i * size;
                    end += size;
                    if (i === indexes -1) {
                        end = str.length;
                    }
                    synapticNDS.send([channel, sender], [ indexes + divider + i + divider + type + str.slice(pos,end), JSON.stringify(sender)], to.toLowerCase());

                }


            }
            console.log("Sending");
            console.log(ev)
        },
        message: function(evt) {
            var event = {
                type: "Not defined"
            };
            if (evt.indexOf(divider) !== -1) {
                var split = evt.split(divider);
                var indexes = Number(split[0]);
                var index = Number(split[1]);
                var type = split[2].charAt(0);
                var data = split[2].slice(1,split[2].length);
                if (type === "o") {
                    videoOfferReceived[index] = data;
                    offerCounter++;
                    if (offerCounter === indexes) {
                        var datastr = videoOfferReceived.join("");
                        event = JSON.parse(datastr);
                        offerCounter = 0;
                        videoOfferReceived = [];
                    }
                }
                else {
                    videoAnswerReceived[index] = data;
                    answerCounter++;
                    if (answerCounter === indexes) {
                        var datastring = videoAnswerReceived.join("");
                        event = JSON.parse(datastring);
                        answerCounter = 0;
                        videoAnswerReceived = [];
                    }
                }
            }
            else {
                event = JSON.parse(evt);
            }
            console.log("Receiving");
            console.log(event);
            if (event.hasOwnProperty("from")) {
                to = (event.from.id) ? event.from.id : event.from;
            }

            switch (event.type) {
                case "nic": {
                    nicArr.push(event);
                    if (nic) {
                        handleNIC();
                    }
                    break;
                }
                case "video-offer": {

                    handleVideoOffer(event);

                    break;
                }
                case "video-answer":
                    handleVideoAnswer(event);
                    break;

            }
        }
    };

    function channelListener(event) {
        signaler.message(event);
    }
    synapticNDS.addChannelLister(channel.toLowerCase(),channelListener);


    function createPeerConnection() {

        console.log("Creating Peer Connection");
        pc = new webkitRTCPeerConnection(null);
        pc.canTrickleIceCandidates = true;

        hasAddTrack = (pc.addTrack !== undefined);

        pc.onicecandidate = handleIceCandidate;
        pc.onremovestream = handleRemoveStream;
        pc.oniceconnectionstatechange = handleIceStateChange;
        pc.onicegatheringstatechange = handleIceGatheringStateChange;
        pc.onsignalingstatechange = handleSignalingStateChange;
        pc.onnegotiationneeded = handleNegotiationNeeded;

        if (hasAddTrack) {
            pc.ontrack = handleTrack;
        }
        else {
            pc.onaddstream = handleAddStream;
        }
    }

    function handleNegotiationNeeded() {
        console.log("*** Negotiation needed");

        console.log("---> Creating offer");
        pc.createOffer().then(function(offer) {
            console.log("---> Creating new description object to send to remote peer");
            return pc.setLocalDescription(offer);
        })
            .then(function() {
                console.log("---> Sending offer to remote peer");
                signaler.send({
                    type: "video-offer",
                    sdp: pc.localDescription
                });
            })
            .catch(reportError);
    }

    function handleTrack(event) {
        console.log("*** Track event");
        remoteStream.srcObject = event.streams[0];
        remoteStream.src = event.streams[0];

    }

    function handleAddStream(event) {
        console.log("*** Stream added");
        remoteStream.srcObject = event.stream;
        remoteStream.src = window.URL.createObjectURL(event.stream);
        //document.getElementById("hangup-button").disabled = false;
    }

    function handleRemoveStream(event) {
        console.log("*** Stream removed");
        closeVideoCall();
    }

    function handleIceCandidate(event) {
        if (event.candidate) {
            console.log("Outgoing ICE candidate: " + event.candidate.candidate);

            signaler.send({
                type: "nic",
                candidate: event.candidate
            });
        }
    }

    function handleIceStateChange(event) {
        console.log("*** ICE connection state changed to " + pc.iceConnectionState);



        switch(pc.iceConnectionState) {

            case "closed":
            case "failed":
            case "disconnected":
                closeVideoCall();
                break;
        }
    }

    function handleSignalingStateChange(event) {
        console.log("*** WebRTC signaling state changed to: " + pc.signalingState);


        switch(pc.signalingState) {
            case "have-remote-offer": {
                handleNIC();
                break;
            }
            case "closed":
                closeVideoCall();
                break;
        }
    }
    function handleIceGatheringStateChange(event) {
        console.log("*** ICE gathering state changed to: " + pc.iceGatheringState);
    }

    function closeVideoCall() {

        console.log("Closing the call");

        // Close the RTCPeerConnection

        if (pc) {
            console.log("--> Closing the peer connection");

            // Disconnect all our event listeners; we don't want stray events
            // to interfere with the hangup while it's ongoing.

            pc.onaddstream = null;  // For older implementations
            pc.ontrack = null;      // For newer ones
            pc.onremovestream = null;
            pc.onnicecandidate = null;
            pc.oniceconnectionstatechange = null;
            pc.onsignalingstatechange = null;
            pc.onicegatheringstatechange = null;
            pc.onnotificationneeded = null;

            // Stop the videos

            if (remoteStream.srcObject) {
                remoteStream.srcObject.getTracks().forEach(function(track){track.stop()});
            }

            if (localVideo.srcObject) {
                localVideo.srcObject.getTracks().forEach(function(track) { track.stop()});
            }

            remoteStream.src = null;
            localVideo.src = null;

            // Close the peer connection

            pc.close();
            pc = null;
            nic = false;
        }

    }



    function invite(invite, localView, remoteView) {
        send = signal.signaler(invite);
        localVideo = document.getElementById(localView);
        remoteStream = document.getElementById(remoteView);
        console.log("Starting to prepare an invitation");
        if (pc) {
            alert("You can't start a call because you already have one open!");
        } else {
            to = invite;
            createPeerConnection(localView, remoteView);



            // Now configure and create the local stream, attach it to the
            // "preview" box (id "local_video"), and add it to the
            // RTCPeerConnection.

            console.log("Requesting webcam access...");

            navigator.webkitGetUserMedia(mediaConstraints, function(localStreamer) {
                console.log("-- Local video stream obtained");
                localVideo.src = window.URL.createObjectURL(localStreamer);
                localVideo.srcObject = localStreamer;

                if (hasAddTrack) {
                    console.log("-- Adding tracks to the RTCPeerConnection");
                    localStreamer.getTracks().forEach(function(track) { pc.addTrack(track, localStreamer)});
                } else {
                    console.log("-- Adding stream to the RTCPeerConnection");
                    pc.addStream(localStreamer);
                }
            }, handleGetUserMediaError);
        }
    }

    this.invite = invite;

    function handleVideoOffer(msg) {
        var localStreaming = null;

        // Call createPeerConnection() to create the RTCPeerConnection.

        console.log("Starting to accept invitation from " );
        createPeerConnection();

        // We need to set the remote description to the received SDP offer
        // so that our local WebRTC layer knows how to talk to the caller.

        var desc = new RTCSessionDescription(msg.sdp);

        pc.setRemoteDescription(desc).then(function () {
            console.log("Setting up the local media stream...");
            return navigator.mediaDevices.getUserMedia(mediaConstraints);
        })
            .then(function(stream) {
                console.log("-- Local video stream obtained");
                localStreaming = stream;
                localVideo.src = window.URL.createObjectURL(localStreaming);
                localVideo.srcObject = localStreaming;

                if (hasAddTrack) {
                    console.log("-- Adding tracks to the RTCPeerConnection");
                    localStreaming.getTracks().forEach(function(track) {
                        pc.addTrack(track, localStreaming)
                    });
                } else {
                    console.log("-- Adding stream to the RTCPeerConnection");
                    pc.addStream(localStreaming);
                }
            })
            .then(function() {
                console.log("------> Creating answer");
                // Now that we've successfully set the remote description, we need to
                // start our stream up locally then create an SDP answer. This SDP
                // data describes the local end of our call, including the codec
                // information, options agreed upon, and so forth.
                return pc.createAnswer();
            })
            .then(function(answer) {
                console.log("------> Setting local description after creating answer");
                // We now have our answer, so establish that as the local description.
                // This actually configures our end of the call to match the settings
                // specified in the SDP.
                return pc.setLocalDescription(answer);
            })
            .then(function() {
                var msg = {
                    type: "video-answer",
                    sdp: pc.localDescription
                };

                // We've configured our end of the call now. Time to send our
                // answer back to the caller so they know that we want to talk
                // and how to talk to us.

                console.log("Sending answer packet back to other peer");
                signaler.send(msg);
                offer = false;
                nic = true;
            })
            .catch(handleGetUserMediaError);
    }

    function handleVideoAnswer(msg) {
        console.log("Call recipient has accepted our call");

        // Configure the remote description, which is the SDP payload
        // in our "video-answer" message.

        var desc = new RTCSessionDescription(msg.sdp);
        pc.setRemoteDescription(desc).catch(reportError);
        nic = true;
    }


    function handleNIC() {
        while(nicArr.length > 0) {
            var candidate = new RTCIceCandidate(nicArr.shift().candidate);

            console.log("Adding received ICE candidate: " + JSON.stringify(candidate));
            pc.addIceCandidate(candidate)
                .catch(reportError);
        }
    }

    function reportError(errMessage) {
        console.log("Error " + errMessage.name + ": " + errMessage.message);
    }

    function handleGetUserMediaError(e) {
        console.log(e);
        switch(e.name) {
            case "NotFoundError":
                alert("Unable to open your call because no camera and/or microphone" +
                    "were found.");
                break;
            case "SecurityError":
            case "PermissionDeniedError":
                // Do nothing; this is the same as the user canceling the call.
                break;
            default:
                alert("Error opening your camera and/or microphone: " + e.message);
                break;
        }

        // Make sure we shut down our end of the RTCPeerConnection so we're
        // ready to try again.

        closeVideoCall();
    }

    this.closeVideoCall = closeVideoCall;

}