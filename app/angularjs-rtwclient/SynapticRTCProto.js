/**
 * Created by pdrevland on 22.07.16.
 */

var SynapticRTCProto = function(localEl, remoteEl) {  // ABSRACT CLASS

    var localElement = document.getElementById(localEl);
    var remoteElement = document.getElementById(remoteEl);
    navigator.userMedia = ( navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);

    this.switch = function(value, errorHandler) {
      var mediaConstraits = (value) ? {
          video: true,
          audio: true
      } :
        {
            //audio: true,
            video: {
                mandatory: {
                    chromeMediaSource: "screen",
                    maxWidth: 1920,
                    maxHeight: 1080
                },
                optional: [{
                    googTemporalLayeredScreencast: true
                }]
            }
        };
        var cab;
        var retur = {
            then: function(cb) {
                cab = cb;
            }
        };
        navigator.userMedia(mediaConstraits, function (stream) {
            localElement.srcObject = stream;
            cab(stream);
        }, errorHandler);
        return retur;
    };

    this.setLocalMedia = function(mediaConstraints, errorHandler) {
        var cab;
        var retur = {
        then: function(cb) {
           cab = cb;
            }
        };
        console.log(navigator.mediaDevices);
        navigator.userMedia(mediaConstraints, function (stream) {
            //localElement.src = window.URL.createObjectURL(stream);
            localElement.srcObject = stream;
            //localElement.muted = false;
            cab(stream);
        }, errorHandler);
        return retur;
    };


    this.setRemoteMediaTrack = function(stream){
        remoteElement.srcObject = stream;
        //remoteElement.src = window.URL.createObjectURL(stream);
    };

    this.setRemoteMedia = function(stream) {
        remoteElement.srcObject = stream;
        //remoteElement.src = window.URL.createObjectURL(stream);
    };

    this.resetElements = function() {
        if (remoteElement.srcObject) {
            remoteElement.srcObject.getTracks().forEach(function(track){track.stop()});
        }

        if (localElement.srcObject) {
            localElement.srcObject.getTracks().forEach(function(track) { track.stop()});
        }
        localElement.src = remoteElement.src = null;
    }

};

