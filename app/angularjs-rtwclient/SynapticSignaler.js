/**
 * Created by pdrevland on 22.07.16.
 */

class SynapticSignaler {
    constructor(snds, messageHandlers, channel) {
        var syn = snds;
        this.send = function(channel, data, to) {
            syn.send(channel, data, to);
        };
        this.message = function(message) {
            messageHandlers(message);
        };
        this.close = function () {
            syn.removeChannelListener(this.message, channel);
            delete this;
        };
        syn.addChannelLister(channel.toLowerCase(), this.message);
    }

}

