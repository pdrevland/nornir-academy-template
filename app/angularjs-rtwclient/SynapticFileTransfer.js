/**
 * Created by pdrevland on 25.07.16.
 */

var SynapticFileTransferSender = function(sdns, channel, sendr, progressCallback, senderId) {
    console.log("Created");
    var local;
    var id = senderId;
    var sendChannel;
    var sender = sendr.toLowerCase();
    var to;
    var file;
    var nic = false;
    var offer = false;
    var progress = 0;
    var chunksize = 16384;
    var size = 0;
    var messageNic = "synapticFileNIC";
    var messageAnswer = "synapticFileANSWER";
    var messageUnauthorized = "synapticFileUNAUTHORIZED";
    var messageOffer = "synapticFileOFFER";
    var messagePreFlight = "synapticFilePREFLIGHT";
    var messagePreFlightResponse = "synapticFilePREFLIGHTRESPONSE";

    var nicArr = [];
    var signaler = new SynapticSignaler(sdns, messageHandler, channel);

    function send(data, to) {
        data.id = id;
        signaler.send(channel, JSON.stringify(data), to.toLowerCase())
    }

    function messageHandler(event) {
        try {
            var message = JSON.parse(event);
            console.log(message);
            if (message.id === id) {
                to = message.from;
                switch (message.type) {
                    case messageAnswer: handleAnswer(message); break;
                    case messageNic: handleICE(message); break;
                    case messageUnauthorized: closeConnection(); break;
                    case messagePreFlightResponse: createConnection(); break;
                    case "received": closeConnection(); break;
                }
            }
        } catch (e) {
            console.log(e)
        }
    }

    function handleICE(message) {
        nicArr.push(message.candidate);
        if (nic) {
            while (nicArr.length > 0) {
                //console.log(nicArr[0]);
                console.log(local.iceGatheringState);
                console.log(local.signalingState);
                var cand = new RTCIceCandidate(nicArr.shift());
                console.log(cand);
                local.addIceCandidate(cand).catch(function(err) {
                    console.log(err)
                });
            }
        }
    }

    function createConnection() {
        local = new webkitRTCPeerConnection(null,null);

        local.canTrickleIceCandidates = true;

        sendChannel = local.createDataChannel('sendDataChannel');

        sendChannel.onopen = onSendChannelStateChanged;
        sendChannel.onclose = onSendChannelStateChanged;
        sendChannel.binarytype = "arraybuffer";

        local.onicecandidate = iceHandler;
        console.log(local.localDescription);
        local.createOffer().then(function(offer) {
            console.log("Offer created");
            return local.setLocalDescription(offer)
        }).then(function() {
            var data = {
                type: messageOffer,
                sdp: local.localDescription,
                from: sender
            };
            console.log("Sending offer");
            send(data,to);
            offer = true;
        },function(err) {
            console.log(err)
        });
    }
    var desc = undefined;
    function handleAnswer(message) {
        console.log("Getting answer");
        console.log(message.sdp);
        desc = new RTCSessionDescription(message.sdp);
        local.setRemoteDescription(desc);
        nic = true;
        if (nicArr.length > 0) {
            handleICE({candidate: nicArr.shift()})
        }
    }

    function onSendChannelStateChanged() {
        if (sendChannel && sendChannel.readyState === "open") {
            if (file) {
                sendData();
            }
            else {
                var txt;
                while (textArr.length > 0) {
                    txt = textArr.shift();
                    sendChannel.send(txt);
                    if (txt === "close") {
                        closeConnection();
                    }
                }
            }
        }

    }

    function sendData() {
        if (progress + chunksize < size) {
            sendChannel.send(file.slice(progress, progress + chunksize));
            progress += chunksize;
        }
        else {
            sendChannel.send(file.slice(progress, size));
            progress = size;
        }
        console.log(progress);
        progressCallback(progress);
        if (progress !== size) {
            sendData();
        }
    }

    function iceHandler(event) {
        if (event.candidate) {
            var data = {
                type: messageNic,
                candidate: event.candidate,
                from: sender
            };
            send(data, to);
        }
    }
    var textArr = [];
    this.signaler = function(sendTo) {
        file = null;
        var data = {
            type: messagePreFlight,
            preflight: {
                type: "text"
            },
            from: sender
        };
        to = sendTo;
        send(data,sendTo);
        return {
            send: function (text) {
                var txt;
                textArr.push(text);
                if (sendChannel && sendChannel.readyState === "open") {
                    txt = textArr.shift();
                    while (txt) {
                        sendChannel.send(txt);
                        if (txt === "close") {
                            closeConnection();
                        }
                        else {
                            txt = textArr.shift();
                        }
                    }
                }
            },
            close: function() {
                var txt;
                textArr.push("close");
                if (sendChannel && sendChannel.readyState === "open") {
                    txt = textArr.shift();
                    while (txt) {
                        sendChannel.send(txt);
                        txt = textArr.shift();
                    }
                    closeConnection();
                }
            }
        }
    };

    this.sendFile = function(sendFile, sendTo) {
        var data = {
            type: messagePreFlight,
            preflight: {
                type: sendFile.type,
                size: sendFile.size,
                name: sendFile.name,
                from: sender
            },
            from: sender
        };
        size = sendFile.size;
        to = sendTo.toLowerCase();
        send(data,to);
        var fileReader = new FileReader();
        fileReader.onload = function() {
            file = this.result;
            console.log("Creating connection");
            //createConnection();
        };
        fileReader.readAsArrayBuffer(sendFile);

    };

    function closeConnection() {
        console.log("Sender Closing");
        if (sendChannel) {
            sendChannel.close();
        }
        if (local) {
            local.close();
        }
        sendChannel = null;
        local = null;
        signaler.close();
    }
    this.closeConnection = closeConnection;

};