/**
 * Created by pdrevland on 22.07.16.
 */


var SynapticVideoCaller = function(snds, channel, localEl, remoteEl, lsender, senderId) {
    var to;
    var synProto;
    var mediaConstraints = {
        audio: true,            // We want an audio track
        video: true             // ...and we want a video track
    };
    var pc;
    var id = senderId;
    var localElement = localEl;
    var remoteElement = remoteEl;
    var hasAddTrack = false;
    var sender = lsender;
    var messageNIC = "synapticVideoCallerNIC";
    var messageVideoOffer = "synapticVideoCallerVO";
    var messageVideoAnswer = "synapticVideoCallerANSWER";

    var signal = new SynapticFileTransferSender(snds, channel, sender, function(){}, id);
    var receiver = new SynapticFileTransferReceiver(snds, channel, sender, function(message) {
        if (message !== "close") {
            var event = JSON.parse(message);

            switch (event.type) {
                case messageNIC: handleNIC(event); break;
                case messageVideoOffer: handleVideoOffer(event); break;
                case messageVideoAnswer: handleVideoAnswer(event); break;
            }
        }
    }, null, id);

    var sending = undefined;

    function send(ev) {
        sending.send(JSON.stringify(ev));
    }

    function createPeerConnection() {

        //console.log("Creating Peer Connection");
        var peerCon = (webkitRTCPeerConnection || mozRTCPeerConnection || msRTCPeerConnection || null);
        if (peerCon) {
            pc = new peerCon(null)
        }
        else {
            return { err: "RTCPeerConnection is not defined" };
        }
        pc.canTrickleIceCandidates = true;

        hasAddTrack = (pc.addTrack !== undefined);

        pc.onicecandidate = handleIceCandidate;
        pc.onremovestream = handleRemoveStream;
        pc.oniceconnectionstatechange = handleIceStateChange;
        pc.onicegatheringstatechange = handleIceGatheringStateChange;
        pc.onsignalingstatechange = handleSignalingStateChange;
        pc.onnegotiationneeded = handleNegotiationNeeded;

        if (hasAddTrack) {
            pc.ontrack = handleTrack;
        }
        else {
            pc.onaddstream = handleAddStream;
        }
    }

    function handleNegotiationNeeded() {
        //console.log("*** Negotiation needed");
        //console.log("---> Creating offer");
        pc.createOffer({
            offerToReceiveAudio: 1,
            offerToReceiveVideo: 1
        }).then(function(offer) {
            console.log("---> Creating new description object to send to remote peer");
            return pc.setLocalDescription(offer);
        })
            .then(function() {
                //console.log("---> Sending offer to remote peer");
                send({
                    type: messageVideoOffer,
                    sdp: pc.localDescription,
                    from: sender
                });
            })
            .catch(reportError);
    }

    function handleTrack(event) {
        //console.log("*** Track event");
        synProto.setRemoteMediaTrack(event.streams[0]);
    }

    function handleAddStream(event) {
        //console.log("*** Stream added");
        synProto.setRemoteMedia(event.stream);
    }

    function handleRemoveStream(event) {
        if (!screenSwitching) {
            console.log("*** Stream removed");
            console.log(event);
            closeVideoCall();
        }
    }

    function handleIceCandidate(event) {
        if(screenSwitching) {
            return;
        }
        if (event.candidate) {
          //  console.log("Outgoing ICE candidate: " + event.candidate.candidate);

            send({
                type: messageNIC,
                candidate: event.candidate,
                from: sender
            });
        }
    }

    function handleIceStateChange(event) {
      //  console.log("*** ICE connection state changed to " + pc.iceConnectionState);
        switch(pc.iceConnectionState) {

            case "closed":
            case "failed":
            case "disconnected":
                console.log(pc.iceConnectionState);
                closeVideoCall();
                break;
            case "completed":
              //  sending.close();
              //  receiver.closeConnection();
              //  sending = null;
              //  signal = null;
              //  receiver = null;
                break;
        }
    }

    function handleSignalingStateChange(event) {
        //console.log("*** WebRTC signaling state changed to: " + pc.signalingState);


        switch(pc.signalingState) {

            case "closed":
                closeVideoCall();
                break;
        }
    }
    function handleIceGatheringStateChange(event) {
        //console.log("*** ICE gathering state changed to: " + pc.iceGatheringState);
        if (pc.iceGatheringState === "completed") {
            //sending.close();
            //signal = null;
        }
    }

    function closeVideoCall() {

        //console.log("Closing the call");

        // Close the RTCPeerConnection

        if (pc) {
          //  console.log("--> Closing the peer connection");

            // Disconnect all our event listeners; we don't want stray events
            // to interfere with the hangup while it's ongoing.

            pc.onaddstream = null;  // For older implementations
            pc.ontrack = null;      // For newer ones
            pc.onremovestream = null;
            pc.onnicecandidate = null;
            pc.oniceconnectionstatechange = null;
            pc.onsignalingstatechange = null;
            pc.onicegatheringstatechange = null;
            pc.onnotificationneeded = null;

            // Stop the videos

            synProto.resetElements();


            // Close the peer connection

            pc.close();
            pc = null;
        }
        if (sending) {
            sending.close();
            sending = null;
        }
        if (signal) {
            signal.closeConnection();
            signal = null;
        }
        if (receiver) {
            receiver.closeConnection();
            receiver = null;
        }
    }

    var screen = false;
    var screenSwitching = false;
    function screenSwitch() {
        screenSwitching = true;
        synProto.switch(screen, handleGetUserMediaError).then(function(stream) {

            if (hasAddTrack) {
                //           console.log("-- Adding tracks to the RTCPeerConnection");
                stream.getTracks().forEach(function(track) { pc.addTrack(track, stream)});
            } else {
                //         console.log("-- Adding stream to the RTCPeerConnection");
                pc.addStream(stream);
            }

        });
    }
    this.screenSwitch = screenSwitch;


    function invite(invite, localView, remoteView, screenCast) {
        sending = signal.signaler(invite);
        localElement = localView || localEl;
        remoteElement = remoteView || remoteEl;
        console.log("Starting to prepare an invitation");
        if (pc) {
            alert("You can't start a call because you already have one open!");
        } else {
            to = invite;
            synProto = new SynapticRTCProto(localElement, remoteElement);
            createPeerConnection();



            // Now configure and create the local stream, attach it to the
            // "preview" box (id "local_video"), and add it to the
            // RTCPeerConnection.

       //     console.log("Requesting webcam access...");
            if (screenCast) {
                screenSwitch();
            }
            else {
            synProto.setLocalMedia(mediaConstraints,handleGetUserMediaError).then(function(stream) {
                if (hasAddTrack) {
         //           console.log("-- Adding tracks to the RTCPeerConnection");
                    stream.getTracks().forEach(function(track) { pc.addTrack(track, stream)});
                } else {
           //         console.log("-- Adding stream to the RTCPeerConnection");
                    pc.addStream(stream);
                }
            //    console.log(stream);
            });
            }

        }
    }

    this.invite = invite;

    function handleVideoOffer(msg) {
        var localStreaming = null;
        if (sending === undefined) {
            sending = signal.signaler(msg.from);
        }
        // Call createPeerConnection() to create the RTCPeerConnection.

        console.log("Starting to accept invitation from " );
        synProto = new SynapticRTCProto(localElement,remoteElement);
        createPeerConnection();
        pc.onnegotiationneeded = function() {};
        // We need to set the remote description to the received SDP offer
        // so that our local WebRTC layer knows how to talk to the caller.

        var desc = new RTCSessionDescription(msg.sdp);

        pc.setRemoteDescription(desc).then(function () {
       //     console.log("Setting up the local media stream...");
            return synProto.setLocalMedia(mediaConstraints,handleGetUserMediaError);
        })
            .then(function(stream) {
         //       console.log("-- Local video stream obtained");
                localStreaming = stream;


                if (hasAddTrack) {
           //         console.log("-- Adding tracks to the RTCPeerConnection");
                    localStreaming.getTracks().forEach(function(track) {
                        pc.addTrack(track, localStreaming)
                    });
                } else {
             //       console.log("-- Adding stream to the RTCPeerConnection");
                    pc.addStream(localStreaming);
                }
            })
            .then(function() {
             //   console.log("------> Creating answer");
                // Now that we've successfully set the remote description, we need to
                // start our stream up locally then create an SDP answer. This SDP
                // data describes the local end of our call, including the codec
                // information, options agreed upon, and so forth.
                return pc.createAnswer();
            })
            .then(function(answer) {
               // console.log("------> Setting local description after creating answer");
                // We now have our answer, so establish that as the local description.
                // This actually configures our end of the call to match the settings
                // specified in the SDP.
                return pc.setLocalDescription(answer);
            })
            .then(function() {
                var msg = {
                    type: messageVideoAnswer,
                    sdp: pc.localDescription,
                    from: sender
                };

                // We've configured our end of the call now. Time to send our
                // answer back to the caller so they know that we want to talk
                // and how to talk to us.

                //console.log("Sending answer packet back to other peer");
                send(msg);
            })
            .catch(handleGetUserMediaError);
    }

    function handleVideoAnswer(msg) {
        //console.log("Call recipient has accepted our call");

        // Configure the remote description, which is the SDP payload
        // in our "video-answer" message.

        var desc = new RTCSessionDescription(msg.sdp);
        pc.setRemoteDescription(desc).catch(reportError);
    }


    function handleNIC(message) {
        var candidate = new RTCIceCandidate(message.candidate);
        //console.log("Adding received ICE candidate: " + JSON.stringify(candidate));
        pc.addIceCandidate(candidate)
            .catch(reportError);
    }

    function reportError(errMessage) {
        console.log("Error " + errMessage.name + ": " + errMessage.message);
    }

    function handleGetUserMediaError(e) {
        console.log(e);
        switch(e.name) {
            case "NotFoundError":
                alert("Unable to open your call because no camera and/or microphone" +
                    "were found.");
                break;
            case "SecurityError":
            case "PermissionDeniedError":
                // Do nothing; this is the same as the user canceling the call.
                break;
            default:
                alert("Error opening your camera and/or microphone: " + e.message);
                break;
        }

        // Make sure we shut down our end of the RTCPeerConnection so we're
        // ready to try again.

        closeVideoCall();
    }

    this.closeVideoCall = closeVideoCall;
};