/**
 * Created by pdrevland on 25.07.16.
 */

var SynapticFileTransferReceiver = function(snds, channel, sendr, fileReadyCallback, optionalAuthorize, senderId) {
    var receive;
    var to;
    var nic = false;
    var file;
    var sender = sendr.toLowerCase();
    var sum = 0;
    var fileArray = [];
    var type = "arraybuffer";
    var messageNic = "synapticFileNIC";
    var messageAnswer = "synapticFileANSWER";
    var messageUnauthorized = "synapticFileUNAUTHORIZED";
    var messageOffer = "synapticFileOFFER";
    var messagePreFlight = "synapticFilePREFLIGHT";
    var messagePreFlightResponse = "synapticFilePREFLIGHTRESPONSE";
    var id = senderId;

    var messageHandler = function(event) {
        try {
            var message = JSON.parse(event);
            console.log(message);
            if (message.id === id) {
                switch (message.type) {
                    case messageOffer:
                        handleOffer(message);
                        break;
                    case messageNic:
                        handleICE(message);
                        break;
                    case messagePreFlight: {
                        if (optionalAuthorize) {
                            if (optionalAuthorize(message.preflight)) {
                                file = message.preflight;
                                send({
                                    type: messagePreFlightResponse,
                                    from: sender
                                }, message.from);
                            }
                            else {
                                var data = {
                                    type: messageUnauthorized,
                                    from: sender
                                };
                                send(data, message.from);
                                closeConnection();
                            }
                        }
                        else {
                            file = message.preflight;
                            if (file.type === "text") {
                                type = file.type;
                            }
                            send({
                                type: messagePreFlightResponse,
                                from: sender
                            }, message.from);
                        }

                    }
                        break;

                }
            }
        } catch (e) {
            console.log(e);
        }
    };
    var signaler = new SynapticSignaler(snds, messageHandler, channel);
    var send = function (data, to) {
        data.id = id;
        signaler.send(channel, JSON.stringify(data), to.toLowerCase())
    };

    function handleOffer(message) {
        to = message.from;

        console.log("Receiving offer");

        receive = new webkitRTCPeerConnection(null,null);
        receive.canTrickleIceCandidates = true;
        receive.onicecandidate = handleNic;
        receive.ondatachannel = handleReceiveDataChannel;


        //var desc = new RTCSessionDescription(message);
        receive.setRemoteDescription(message.sdp).then(function() {
            return receive.createAnswer()
        },handleError).then(function(answer) {
            return receive.setLocalDescription(answer)
        },handleError).then(function() {
            var data = {
                type: messageAnswer,
                sdp: receive.localDescription,
                from: sender
            };
            console.log("Sending answer");
            send(data, to);
            nic = true;
            if (nicArr.length > 0) {
                handleICE({candidate: nicArr.shift()});
            }
        },handleError);
    }

    var nicArr = [];
    function handleICE(message) {
        nicArr.push(message.candidate);
        if (nic) {
            while (nicArr.length > 0) {
                receive.addIceCandidate(nicArr.shift())
            }
        }
    }

    function handleNic(event) {
        if (event.candidate) {
            var data = {
                type: messageNic,
                candidate: event.candidate,
                from: sender
            };
            send(data, to)
        }
    }

    var receiveChannel;
    function handleReceiveDataChannel(event) {
        receiveChannel = event.channel;
        receiveChannel.onopen = function () {
            console.log("Open channel");
        };
        if (type === "arraybuffer") {
            receiveChannel.binaryType = "arraybuffer";
            receiveChannel.onmessage = onMessageCallback;
        }
        else {
            receiveChannel.onmessage = function(message) {
                if (message.data === "close") {
                    closeConnection();
                }
                else {
                    fileReadyCallback(message.data);
                }
            }
        }
        receiveChannel.onclose = function () {
            closeConnection();
        }
    }

    function onMessageCallback(message) {
        sum += message.data.byteLength;
        fileArray.push(new DataView(message.data));
        fileReadyCallback(sum, null);
        console.log(sum);
        if (sum >= file.size) {
            send({type: "received", from: sender}, to);
            var blob = new Blob(fileArray, {type: file.type});
            fileReadyCallback(sum, blob, to);
            sum = 0;
            fileArray = [];
            closeConnection();
        }
    }

    function closeConnection() {
        console.log("Closing Connection");
        if (receiveChannel) {
            receiveChannel.close();
        }
        if (receive) {
            receive.close();
        }
        receive = null;
        receiveChannel = null;
        signaler.close();
    }

    this.closeConnection = closeConnection;

    function handleError(err) {
        console.log(err)
    }

};