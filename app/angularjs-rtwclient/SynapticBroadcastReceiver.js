/**
 * Created by pdrevland on 25.07.16.
 */

var SynapticBroadCastReceiver = function(snds, sender, channel, remoteview,selfview, hasBroadCastCb) {
    var synProto;
    var bc = undefined;
    var nic = false;
    var to;
    var stream;

    var messageHandler = function(message) {
        var event = JSON.parse(message);
        console.log(event);
        to = event.from;
        switch (event.type) {
            case "video-offer": handleVideoOffer(event); break;

            case "link-relay": handleLinkRelay(event); break;
            case "nic": handleNIC(event); break;
            case "has-broadcast": hasBroadCastCb(event);
        }
    };

    var signaler = new SynapticSignaler(snds, messageHandler, channel);

    function send(data, to) {

        signaler.send(channel, JSON.stringify(data), to.toLowerCase())
    }

    function handleVideoOffer(message) {

        bc = new webkitRTCPeerConnection(null);
        bc.canTrickleIceCandidates = true;

        synProto = new SynapticRTCProto(selfview, remoteview);
        bc.onicecandidate = handleIceCandidate;
        bc.onaddstream = handleAddStream;
        var sendTo = message.from;
        bc.onnegotiationneeded = function (event) {};

        var desc = new RTCSessionDescription(message.sdp);
        bc.setRemoteDescription(desc).then(function() {
            return bc.createAnswer()
        }).then(function(answer) {
            return bc.setLocalDescription(answer)
        }).then(function() {
            var data = bc.localDescription;
            send(data, sendTo);
        });
        /*
        bc.setRemoteDescription(desc).then(function() {
            return synProto.setLocalMedia({video: true, audio: false}, function (err) {
                return {err: err}
            })
        }).then(function(localStrem) {
            if (bc.addTrack !== undefined) {
                localStrem.getTracks().forEach(function(track) {
                    bc.addTrack(track, localStrem)
                });
            }
            else {
                bc.addStream(localStrem);
            }
        }).then(function() {
            return bc.createAnswer()
        }).then(function(answer) {
            return bc.setLocalDescription(answer)
        }).then(function() {
            var data = bc.localDescription;
            to = message.from;
            send(data, message.from);
        }).catch(function(err) {
            console.log(err);
        })*/
    }

    function handleNIC(message) {
        to = message.from;
        var candidate = new RTCIceCandidate(message.candidate);
        bc.addIceCandidate(candidate);

    }

    var ice = [];
    var ice2 = [];
    function handleIceCandidate(event) {
        if (event.candidate) {
        send({
            type: "nic",
            candidate: event.candidate,
            from: sender
        }, to);
        }
    }

    function handleAddStream(event) {
        stream = event.stream;
        synProto.setRemoteMedia(stream);
    }

    this.subscribe = function(broadCaster) {
        send({
            type: "subscriber",
            from: sender
        }, broadCaster);
    }


};