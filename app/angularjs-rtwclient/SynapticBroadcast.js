/**
 * Created by pdrevland on 22.07.16.
 */

var SynapticBroadCast = function(snds,channel,sender) {
    var watched = false;
    var watchers = [];
    watchers[sender] = undefined;
    var watcher;
    var stream;
    var lastWatcher;
    var broadcaster;
    this.hasBroadcast = false;
    var sdp;

    var bc;

    var synProto;

    var messageHandler = function (event) {
        var message = JSON.parse(event);
        console.log(message);
        switch(message.type) {
            case "subscriber": {
                if (watched) {
                    handleLinking(message);
                }
                else {
                    handleLinkMan(message);
                }
                break;
            }
            case "unsubscriber": {
                handleUnsubscribe(message);
                break;
            }
            case "missing-link": {
                handleMissingLink(message);
                break;
            }
            case "nic-ready": {
                handleNicReady(message);
                break;
            }
            case "answer": handleAnswer(message); break;
            case "negotiation": handleNegotiation(message); break;

        }
    };

    var signaler = new SynapticSignaler(snds, messageHandler, channel);

    var send = function(data, to) {
        signaler.send(channel, JSON.stringify(data), to.toLowerCase());
    };

    var handleLinking = function(message) {
        var data = {
            from: sender,
            type: "link-relay",
            to: lastWatcher
        };
        send(data, message.from);
        watchers[message.from] = undefined;
        watchers[lastWatcher] = message.from;
    };

    var handleLinkMan = function(message) {
        watcher = message.from;
        watchers[watcher] = undefined;
        watched = true;
        setUpWatcher(watcher);
    };

    var handleMissingLink = function(message) {
        var link = message.link;
        var toLink;
        var length = 0;
        for (var prop in watchers) {
            length++;
            if (prop === link) {
                toLink = watchers[prop];
            }
        }
        watchers[toLink] = message.from;
        delete watchers[link];
        if (length === 0) {
            watched = false;
        }
        if (toLink === sender) {
            watcher = message.from;
        }
        var data = {
            from: sender,
            type: "link-relay",
            to: toLink
        };
        send(data, message.from);
    };

    var handleUnsubscribe = function(message) {
        var switchLink = message.link;
        var toLink = message.from;
        var toNewLink;
        for (var link in watchers) {
            if (watchers[link] === toLink) {
                watchers[link] = switchLink;
                toNewLink = link;
            }
        }
        if (switchLink === undefined) {
            if (toNewLink === sender) {
                watched = false;
            }
        }
        else {
            var data = {
                from: sender,
                type: "link-relay",
                to: toNewLink
            };
            send(data,switchLink)
        }
        delete watchers[toLink];
    };

    this.startBroadcast = function(localView, broadcastr) {
        broadcaster = broadcastr;
        synProto = new SynapticRTCProto(localView);
        synProto.setLocalMedia({audio: true, video: true}, function(err) {return{err: err}}).then(function(stram) {
            stream = stram;
            var peerCon = (webkitRTCPeerConnection || mozRTCPeerConnection || msRTCPeerConnection || null);
            if (peerCon) {
                bc = new peerCon(null)
            }
            else {
                return { err: "RTCPeerConnection is not defined" };
            }

            bc.canTrickleIceCandidates = true;
            bc.onicecandidate = handleIceCandidate;
            if (bc.addTrack !== undefined) {
                bc.ontrack = handleTrack;
            }
            else {
                bc.onaddstream = handleAddStream;
            }



       /*     pc.onremovestream = handleRemoveStream;
            pc.oniceconnectionstatechange = handleIceStateChange;
            pc.onicegatheringstatechange = handleIceGatheringStateChange;
            pc.onsignalingstatechange = handleSignalingStateChange;

*/
           // bc.onnegotiationneeded = handleNegotiation;

            bc.createOffer().then(function(offer) {
                return bc.setLocalDescription(offer)
            }).then(function() {
                sdp = bc.localDescription;
            });
        });
        this.hasBroadcast = true;
    };

    function handleNegotiation() {
        bc.createOffer().then(function(offer) {
            console.log("---> Creating new description object to send to remote peer");
            return bc.setLocalDescription(offer);
        })
        .then(function() {
            console.log("---> Sending offer to remote peer");
            send({
                type: "video-offer",
                sdp: bc.localDescription,
                from: sender
            },watcher);
            sdp = bc.localDescription;
        })
        .catch(function(err) {
            console.log(err)
        });
    }

    function handleAddStream(event) {
        console.log("*** Stream added");
        //synProto.setRemoteMedia(event.stream);
        console.log(event);
        //document.getElementById("hangup-button").disabled = false;
    }
    this.invite = function (to) {
        if (this.hasBroadcast) {
            send({
                type: "has-broadcast",
                from: broadcaster
            },to)
        }
    };

    function setUpWatcher(watcher) {
        var data =  {
            type: "video-offer",
            sdp: sdp,
            link: sender,
            from: sender
        };
        send(data, watcher);

    }

    function handleNicReady(message) {
        while (ice.length > 0) {
            send({
                type: "nic",
                candidate: ice.shift()
            }, message.from);
        }
    }

    var ice = [];

    function handleIceCandidate(event) {
        if (event.candidate) {
            var data = {
                type: "nic",
                candidate: event.candidate,
                from: sender
            };
            send(data,watcher)
        }
    }

    function handleAnswer(message) {

        //var desc = new RTCSessionDescription(message.sdp);
        var desc = new RTCSessionDescription(message);
        bc.setRemoteDescription(desc);
    }

    function handleTrack(event) {
        console.log(event);
    }
};