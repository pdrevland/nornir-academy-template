# Nornir Academy Project Template
This project is the skeleton project for angularJS with the Nornir javaScript framework

This is the base for every demo projects on our site, and it is built on the angular-seed project

## Prerequisites

For this project you need to have installed the node.js and package manager.
Download and install [node.js](http://nodejs.org/) 

After installation of node.js and npm(bundled with node.js) you need to install gulp task manager globally
```
npm install -g gulp
```
Download this project from https://pdrevland@bitbucket.org/pdrevland/nornir-academy-template.git

## Structure

The structure of the project is as follows
```
    app/                    ---> Application folder
        bower_components/   ---> Bower dependencies
        components/         ---> View/Controller Modules
        constants/          ---> Folder with constants declarations
        factory/            ---> Folder with angular factory objects
        sass/               ---> Folder with sass files and css
        index.html          ---> Web entry file
        app.js              ---> Application entry file
        all.js              ---> Concated application js 
    node_modules/           ---> Development dependencies
```

## Gulp built in tasks

Run 
```
gulp <task>
```
to make development easier on your projects.

We provide to following tasks:

* jsConcat --> concatenates all your application js to all.js
* sass --> concatenates all your sass files to nac.css
* watch --> watches the changes in your application js and sass files and runs jsConcat and sass
* webserver --> starts a local server on your machine on port 8100. If the --port argument is given it uses that port
* module --> creates a new module in the components folder with predefined files and templates. you must specify the --name for your module
* factory ---> creates a new module in the factory folder. You must specify the --name for your new factory
* create-sass ---> creates a new directory in app/sass and imports it to the main nac.scss. You must specify the --name for your directory

We try to follow John Papas [styleguide](https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md) for the modules and factories in our template

## Eslintr

The project comes with an angular rule defining plugin for [Eslint](http://eslint.org/)

## Sass 

The project is ready to go with use of sass. The main scss file imports the bootstrap framework. The variables for the project can be found in the _variables.scss in the app/sass/ directory
Use the gulp task "create-sass" to generate easier to manage styles


## Nornir framework

A deeper look in the API can be found here

Basic usage of the framework is

```
var nornir = new Nornir(configuration);
nornir.registerListener('field1', field1Listener);

// OR register a global listener
nornir.registerListener('*', globalListener);

// start listening for updates
nornir.connect();
```


