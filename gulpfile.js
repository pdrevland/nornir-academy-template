/**
 * Created by pdrevland on 07.07.16.
 */

var gulp = require('gulp');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var sh = require('shelljs');
var argv = require('yargs').argv;
var wrap = require('gulp-wrapped-html');
var json = require('jsonfile');
var dep = require('yargs').demand(1).argv;
var del = require('del');
var app = '';
var dir = '';
var prompt = require('prompt');

var paths = {
    sass: ['app/sass/nac.scss'],
    paths: [
        'app/**!(bower_components)/**/*.module.js',
        'app/**!(bower_components)/**/*.controller.js',
        'app/**!(bower_components)/**/*.directive.js',
        'app/**!(bower_components)/**/*.service.js',
        'app/**!(bower_compnents)/**/*.specs.js',
        'app/**!(bower_components)/**/**.factory.js'
    ]};
var bowercp = 'app/bower_components/';
var fw = 'app/angularjs-rtwclient/*.js';
var buildPaths = [
    bowercp + 'jquery/dist/jquery.js',
    bowercp + 'bootstrap-sass/assets/javascripts/bootstrap.js',
    bowercp + 'angular/angular.js',
    bowercp + 'angular-ui-router/release/angular-ui-router.js',
    bowercp + 'angular-animate/angular-animate.js'
];

var cssPaths = [
    bowercp + 'html5-boilerplate/dist/css/normalize.css',
    bowercp + 'html5-boilerplate/dist/css/main.css',
    'app/sass/nac.css'
];
var webserver = require('gulp-webserver');

gulp.task('webserver', function() {
    gulp.src('./app/')
        .pipe(webserver({
            livereload: true,
            directoryListing: false,
            open: true,
            port: argv.port || 8210,
            fallback: './app/index.html'
            //https: true
        }));
});

gulp.task('jsConcat', function() {
    return gulp.src(paths.paths)
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./app/'));
});

gulp.task('watch', function() {
    gulp.watch(paths.paths, ['jsConcat']);
    gulp.watch(paths.sass, ['sass']);
});


gulp.task('dependencies', function() {
    return gulp.src(buildPaths)
        .pipe(concat('dependencies.js'))
        .pipe(gulp.dest('./app/'));
});

gulp.task('buildCss', ['sass'], function() {
    return gulp.src(cssPaths)
        .pipe(concat('cssall.css'))
        .pipe(gulp.dest('./app/'));
});

gulp.task('module', function(done) {
    sh.mkdir('app/components/' + argv.name);
    var module = '(function() {\n\t' +
        '\'use strict\'' +
        '\n\t angular.module(\'' + argv.name.toLowerCase() + '\', []);' +
        '\n})();';
    										var controller = '(function() {' +
        '\n\tfunction ' + argv.name.charAt(0).toUpperCase() + argv.name.slice(1) +
        'Controller(){\n' +
        '\t\tvar vm = this;\n' +
        '}\n\t' +
        argv.name.charAt(0).toUpperCase() + argv.name.slice(1) + 'Controller.$inject = [\n' +
        '\t\n\t' +
        '];\n' +
        '\tangular.module(\'' + argv.name.toLowerCase() + '\').controller(\'' + argv.name.charAt(0).toUpperCase() +
        argv.name.slice(1) + 'Controller\', ' + argv.name.charAt(0).toUpperCase() + argv.name.slice(1) + 'Controller);' +
        '\n})();';
    var html = '<div></div>';
    var directive = '(function() {' +
        '\n\tfunction ' + argv.name.charAt(0).toUpperCase() + argv.name.slice(1) +
        'Directive() {\n' +
        '\t\treturn {\n' +
        '\t\t\t restrict: \'E\',\n' +
        '\t\t\t templateUrl: \'' + argv.name.toLowerCase() + '/' + argv.name.toLowerCase() + '.html\',\n' +
        '\t\t\t controller: \'' + argv.name.charAt(0).toUpperCase() + argv.name.slice(1) + 'Controller\',\n' +
        '\t\t\t controllerAs: \'' + argv.name + 'Ctrl\'\n' +
        '\t\t}\n' +
        '\t}\n' +
        '\tangular.module(\'' + argv.name.toLowerCase() + '\').directive(\'' + argv.name + '\',' + argv.name.charAt(0).toUpperCase() +
        argv.name.slice(1) + 'Directive);' +
        '\n})();';
    var spec = 'describe(\'Testing Module: ' + argv.name + '\', function() {' +
        '\n})';
    sh.config.silent = true;
    sh.echo(module).to('./app/components/' + argv.name + '/' + argv.name.toLowerCase() + '.module.js');
//sh.exec("echo " +module + " >> ./www/" + argv.name + "/" + argv.name.toLowerCase() + ".module.js", opt);
    sh.echo(controller).to('./app/components/' + argv.name + '/' + argv.name.toLowerCase() + '.controller.js');
    sh.echo(html).to('./app/components/' + argv.name + '/' + argv.name + '.html');
    sh.echo(directive).to('./app/components/' + argv.name + '/' + argv.name + '.directive.js');
    sh.echo(spec).to('./app/components/' + argv.name + '/' + argv.name + '.spec.js');
    done();
});

gulp.task('factory', function(done) {
    sh.mkdir('app/factory/' + argv.name);
    var module = '(function() {' +
        '\n\tangular.module(\'' + argv.name.toLowerCase() + '\', []);' +
        '\n})();';
    										var factory = '(function() {' +
        '\n\tfunction ' + argv.name.split('')[0].toUpperCase() + argv.name.slice(1) +
        'Factory(){\n' +
        '\t\tvar factory = {};\n\n' +
        '\t\treturn factory;' +
        '\n\t}\n\t' +
        argv.name.split('')[0].toUpperCase() + argv.name.slice(1) + 'Factory.$inject = [\n' +
        '\n\t' +
        '];\n\t' +
        'angular.module(\'' + argv.name.toLowerCase() + '\').factory(\'' + argv.name.split('')[0].toUpperCase() +
        argv.name.slice(1) + 'Factory\', ' + argv.name.split('')[0].toUpperCase() + argv.name.slice(1) + 'Factory);' +
        '\n})();';
    var spec = 'describe(\'Testing Factory: ' + argv.name + '\', function() {' +
        '\n})';
    sh.echo(module).to('./app/factory/' + argv.name + '/' + argv.name.toLowerCase() + '.module.js');
    sh.echo(factory).to('./app/factory/' + argv.name + '/' + argv.name.toLowerCase() + '.factory.js');
    sh.echo(spec).to('./app/factory/' + argv.name + '/' + argv.name + '.spec.js');
    done();
});

gulp.task('create-sass', function(done) {
    if (argv.name) {
        sh.mkdir('app/sass/' + argv.name);
        var sass = '// ' + argv.name + '.scss';
        sh.echo(sass).to('app/sass/' + argv.name + '/' + argv.name + '.scss');
        sh.echo(sh.cat('app/sass/nac.scss') + '\n@import \'' + argv.name + '/' + argv.name + '.scss\';').to('app/sass/nac.scss');
        done();
    }
    else {
        sh.echo('No name provided');
        done();
    }
});

gulp.task('sass', function(done) {
    gulp.src('./app/sass/nac.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./app/sass/'))
        .on('end', done);
});

function createSassTemplate() {
    sh.cp(bowercp + 'bootstrap-sass/assets/stylesheets/bootstrap/_variables.scss', 'app/sass/variables.scss');
    sh.echo('\n@import \'variables.scss\';' +
        '\n@import ' + '\'../bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss\';').to(dir + 'app/sass/nac.scss');
}

gulp.task('clean', function() {
    return del([
        'app/{!angularjs-rtwclient, !bower_components}**/**'
    ]);
});

gulp.task('init', ['clean'], function() {
    var appName = 'nic';
    var title = 'Nornir Academy Template';
    prompt.start();
    prompt.get([{
        name: 'title',
        description: 'Application Title',
        required: false
        },
        {
            name: 'name',
            description: 'Application shortname e.g. \'myApp\'',
            required: false
        }
    ], function(err,result) {
        if (!err) {
            sh.exec('bower install');
            appName = result.name;
            title = result.title;
            dir = '.' + '/';
            sh.mkdir(dir + 'app/factory');
            sh.mkdir(dir + 'app/sass');
            sh.mkdir(dir + 'app/constants');
            sh.mkdir(dir + 'app/components');
            sh.echo(' ').to(dir + 'app/sass/nac.scss');
            if (argv.hasOwnProperty('a') || argv.hasOwnProperty('appName')) {
                appName = argv.a || argv.appName;
            }
            if (argv.hasOwnProperty('t') || argv.hasOwnProperty('title')) {
                title = argv.t || argv.title;
            }
            sh.echo('<!DOCTYPE html>\n' +
                '<!--[if lt IE 7]>      <html lang="en" ng-app="'+ appName +'" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->\n' +
                '<!--[if IE 7]>         <html lang="en" ng-app="'+ appName +'" class="no-js lt-ie9 lt-ie8"> <![endif]-->\n' +
                '<!--[if IE 8]>         <html lang="en" ng-app="'+ appName +'" class="no-js lt-ie9"> <![endif]-->\n' +
                '<!--[if gt IE 8]><!--> <html lang="en" ng-app="'+ appName +'" class="no-js"> <!--<![endif]-->\n' +
                '\t<head>\n' +
                '\t\t<meta charset="utf-8">\n' +
                '\t\t<meta http-equiv="X-UA-Compatible" content="IE=edge">\n' +
                '\t\t<title>'+ title +'</title>\n' +
                '\t\t<meta name="description" content="">\n' +
                '\t\t<meta name="viewport" content="width=device-width, initial-scale=1">\n' +
                '\t\t<link rel="stylesheet" href="bower_components/html5-boilerplate/dist/css/normalize.css">\n' +
                '\t\t<link rel="stylesheet" href="bower_components/html5-boilerplate/dist/css/main.css">\n' +
                '\t\t<script src="bower_components/html5-boilerplate/dist/js/vendor/modernizr-2.8.3.min.js"></script>\n' +
                '\t</head>\n' +
                '\t<body>\n' +
                '\t\t<script src="bower_components/angular/angular.js"></script>\n' +
                '\t\t<script src="bower_components/angular-route/angular-route.js"></script>\n' +
                '\t\t<script src="app.js"></script>\n' +
                '\t</body>\n' +
                '</html>\n'
            ).to(dir + 'app/index.html');
            sh.echo('\'use strict\';' +
                '\nangular.module(\''+ appName + '\', [ ' +
                '\n\t\'ui.router\'' +
                '\n]);').to(dir + 'app/app.js');
            createSassTemplate();
        }
        else {
            console.log(err);
        }
    });
});

function createInsertObject(element, last) {
    var insertObject = {
        selector: '',
        insert: ''
    };
    var slice = element.slice(element.lastIndexOf('.') + 1, element.length);
    if (element.charAt(0) === '.') {
        element = element.replace('.','');
    }
    if (slice === 'js') {
        insertObject.selector = 'body';
        insertObject.insert = '\t<script src="' + last  + element +'"></script>\n';
    }
    else if (slice === 'css') {
        insertObject.selector = 'head';
        insertObject.insert = '\t<link rel="stylesheet" href="' + last + element + '">\n';
    }
    else if (slice === 'scss') {
        sh.echo(sh.cat('app/sass/nac.scss') + '\n@import \'../' + last + '/' + element + '\';').to('app/sass/nac.scss');
    }
    console.log(slice);
    return insertObject;
}

function insertStream(insertObjects) {
    if (insertObjects.length !== 0) {
        var insertObject = insertObjects.shift();
        var indexedHtml = sh.cat('./app/index.html');
        sh.rm('./app/index.html');
        sh.echo(insertObject.insert).to('./index.html');
        gulp.src('./index.html')
            .pipe(wrap(indexedHtml,insertObject.selector))
            .pipe(gulp.dest('./app/'))
            .on('end', function () {
                insertStream(insertObjects);
            });
    }
    else {
        sh.rm('./index.html');
    }
}

gulp.task('bowerInstall', function(done) {
    var dependency = dep.d;
    var save = (dep.s) ? '-S' : '';
    sh.exec('bower install ' + save + dependency, function(exit, stdout, stderr) {
        var last = stdout.split(' ').pop().replace('\n', '');
        if (exit == 0) {
            json.readFile(last + '/.bower.json', function(err, obj) {
                last = last.replace('app/', '');
                if (!err) {
                    if (!obj.hasOwnProperty('main')) {
                        return;
                    }
                    var main = obj['main'];
                    var insertArray = [];
                    if (Array.isArray(main)) {
                        main.forEach(function(mainElement) {
                            insertArray.push(createInsertObject(mainElement, last));
                        });
                    }
                    else {
                        insertArray.push(createInsertObject(main, last));
                    }
                    insertStream(insertArray);
                }
                else {
                    console.log(err);
                    done();
                }
            });
        }
        else {
            console.log(stderr);
            console.log(stdout);
            console.log(exit);
            done();
        }
    });

});
